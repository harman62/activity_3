import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class McDtestCases {

	//Location of chromedriver file
    final String CHROMEDRIVER_LOCATION = "/Users/harman/Desktop/chromedriver";
   
    //Website url we want to test
    final String URL_TO_TEST = "https://www.mcdonalds.com/ca/en-ca.html";
    
    //Global declarations
    WebDriver driver;  

	@Before
	public void setUp() throws Exception {
		
		//Setting up the Selenium
	     System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_LOCATION);
	                
	     driver = new ChromeDriver();
	                
	     driver.get(URL_TO_TEST);
	}

	@After
	public void tearDown() throws Exception {
		
            Thread.sleep(2000);
            driver.close();

	}


	@Test
	public void testTitle() {
        // TestCase1:  Title of the subscription feature is “Subscribe to my Mcd’s”
        
        //1. Get the title by fetching the class
        WebElement title = driver.findElement(By.cssSelector("h2.click-before-outline"));
        String titleText = title.getText();
        
        //String titleText = title.getText().substring(0,21);
        System.out.println(titleText);
        
        String expectedResult = "Subscribe to My McD’s®";
        
        
        assertEquals(expectedResult,titleText);
        
    }

	@Test
	public void testPositive() {
		WebElement inputBox1 = driver.findElement(By.id("firstname2"));
		inputBox1.sendKeys("harman");
		WebElement inputBox2 = driver.findElement(By.id("email2"));
		inputBox2.sendKeys("harmankaur.id@gmail.com");
		WebElement inputBox3 = driver.findElement(By.id("postalcode2"));
		inputBox3.sendKeys("M9V");
		WebElement button = driver.findElement(By.cssSelector("form#tile-form button"));
		button.click();
//		WebElement x = driver.findElement(By.cssSelector("iframe button"));
//		//WebElement buttonVerify = driver.findElement(By.id("recaptcha-verify-button"));
//		x.click();
//		System.out.println("success");
//		WebElement outputBox = driver.findElement(By.className("rc-imageselect-error-select-more"));
//		String actualOutput = outputBox.getText();
//		System.out.println(actualOutput);
//        
//		//fail("Not yet implemented");
//		assertEquals("Please select all matching images.", actualOutput);
	
	}
	
	@Test
	public void testNegative() {
		WebElement inputBox1 = driver.findElement(By.id("firstname2"));
		inputBox1.sendKeys("");
		WebElement inputBox2 = driver.findElement(By.id("email2"));
		inputBox2.sendKeys("");
		WebElement inputBox3 = driver.findElement(By.id("postalcode2"));
		inputBox3.sendKeys("");
		WebElement button = driver.findElement(By.cssSelector("form#tile-form button"));
		button.click();
		
	
	}

}
