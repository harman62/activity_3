## Activity3 : Testing Website

#### Submitted By:

Name: *Harman Kaur*

Student Id: *C0744086*

###Test Cases:
** TC1:  Title of the subscription feature is “Subscribe to my Mcd’s”**

  Expected Outcome:
  <ul>
  <li>The title of the section should be: “Subscribe to My McD’s”</li>

**TC2:  Email signup - happy path.**
 
 Inputs:
 
 <ul>
 <li>User enters a first name</li>
 <li>User enters an email address</li>
 <li>User enters first 3 digits of a valid postal code (letter-number-letter)</li>
 <li>User presses subscribe button</li>
 
 Expected Output:
 <ul>
 <li>User sees a Capcha Image</li>
 
 **TC3: Email signup -  negative case**
 
 Inputs:

 <ul>
 <li>User does not enter a first name</li>
 <li>User does not enter a last name</li>
 <li>User does not enter an email</li>
 <li>User presses subscribe</li>
 
  Expected Output:
 <ul>
 <li>User sees a Capcha Image</li>
 




 

